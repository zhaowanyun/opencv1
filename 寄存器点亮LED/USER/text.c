#include "sys.h"
#include "usart.h"		
#include "delay.h"	
#include "led.h"
void A_light()
{
	GPIOA_ODR=0x0<<5;		//PA5低电平
	GPIOB_ODR=0x1<<9;		//PB9高电平
	GPIOC_ODR=0x1<<14;		//PC14高电平
}
void B_light()
{
	GPIOA_ODR=0x1<<5;		//PA5低电平
	GPIOB_ODR=0x0<<9;		//PB9高电平
	GPIOC_ODR=0x1<<14;		//PC14高电平
}
void C_light()
{
	GPIOA_ODR=0x1<<5;		//PA5低电平
	GPIOB_ODR=0x1<<9;		//PB9高电平
	GPIOC_ODR=0x0<<14;		//PC14高电平
}
int main(void)
{				  
	Stm32_Clock_Init(9);//系统时钟设置
	delay_init(72);	  	//延时初始化
	LED_Init();		  	//初始化与LED连接的硬件接口
	
	while(1)
	{
	 A_light();
	 delay_ms(1000);
		 B_light();
		delay_ms(1000);
		 C_light();
		delay_ms(1000);
	}	 
}
