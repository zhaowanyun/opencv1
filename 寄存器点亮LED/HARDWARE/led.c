#include "led.h"
void LED_Init(void)
{

  RCC->APB2ENR|=1<<2;			//APB2-GPIOA外设时钟使能
	RCC->APB2ENR|=1<<3;			//APB2-GPIOB外设时钟使能	
	RCC->APB2ENR|=1<<4;			//APB2-GPIOC外设时钟使能
	GPIOA->CRL&=0xFF0FFFFF;		//设置位 清零	
	GPIOA->CRL|=0X00200000;		//PA5推挽输出
	GPIOA->ODR|=1<<5;			//设置PA5初始灯为灭
	
	GPIOB->CRH&=0xFFFFFF0F;		//设置位 清零	
	GPIOB->CRH|=0x00000020;		//PB9推挽输出
	GPIOB->ODR|=0x1<<9;			//设置初始灯为灭
	
	GPIOC->CRH&=0xF0FFFFFF;		//设置位 清零
	GPIOC->CRH|=0x02000000;   	//PC14推挽输出
	GPIOC->ODR|=0x1<<14;			//设置初始灯为灭	

}

